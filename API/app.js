var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var authRouter = require('./routes/auth');
var profileRouter = require('./routes/profile');
var quoteRouter = require('./routes/quote');
var historyRouter = require('./routes/history');
var registerRouter = require('./routes/register');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
})

var mongoose = require('mongoose');

var userSchema = mongoose.Schema({ username: 'string', password: 'string'}, {collection: 'users'});
var usersModel = mongoose.model('users', userSchema);
app.set('users', usersModel)

var profileSchema = mongoose.Schema({ username: 'string', firstName: 'string', lastName: 'string', address1: 'string', address2: 'string', city: 'string', state: 'string', zipcode: 'string'}, {collection: 'profiles'});
var profileModel = mongoose.model('profiles', profileSchema);
app.set('profiles', profileModel)

var historySchema = mongoose.Schema({username: 'string', gallonRequested: Number, address: 'string', date: 'string', suggested: Number, total: Number}, {collection: 'history'});
var historyModel = mongoose.model('history', historySchema);
app.set('history', historyModel)

app.use('/api/auth', authRouter);
app.use('/api/profile', profileRouter);
app.use('/api/quote', quoteRouter);
app.use('/api/history', historyRouter);
app.use('/api/register', registerRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
