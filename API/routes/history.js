var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');

router.post('/get', function(req, res, next) {
  mongoose.connect('mongodb://localhost/eve', {useNewUrlParser: true});
  var db = mongoose.connection;

  console.log('Grab quote history associated with ' + req.body.username + ' from database');

  var history = req.app.get('history');

  history.find({
    username: req.body.username
  }, function(err, data) {
    mongoose.connection.close();
    res.json({
      history: data
    });
  });
});

router.post('/post', function(req, res, next) {
  mongoose.connect('mongodb://localhost/eve', {useNewUrlParser: true});
  var db = mongoose.connection;

  console.log('Send quote history associated with ' + req.body.username + ' to database');

  var history = req.app.get('history');

  console.log(req.body.quote);

  history.create({
    username: req.body.username,
    gallonRequested: req.body.quote.gallonRequested,
    address: req.body.quote.address,
    date: req.body.quote.date,
    suggested: req.body.quote.suggested,
    total: req.body.quote.total
  }, function(err, data) {
    mongoose.connection.close();

    var result = '';

    if(err)
      result = 'fail';
    else {
      result = 'success';
    }

    res.json({
      result: result
    });
  });
});

module.exports = router;
