var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');

router.post('/get', function(req, res, next) {
  mongoose.connect('mongodb://localhost/eve', {useNewUrlParser: true});
  var db = mongoose.connection;

  console.log('Grab profile associated with ' + req.body.username + ' from database');

  var profiles = req.app.get('profiles');

  profiles.find({
    username: req.body.username
  }, function(err, data) {
    mongoose.connection.close();
    if (data.length > 0)
      res.json(data[0]);
    else
      res.json({
        username: '',
        firstName: '',
        lastName: '',
        address1: '',
        address2: '',
        city: '',
        state: 'Select State',
        zipcode: ''
      });
  });
});

router.post('/post', function(req, res, next) {
  mongoose.connect('mongodb://localhost/eve', { useNewUrlParser: true});

  var result = '';
  var profiles = req.app.get('profiles');

  var username = req.body.username;
  var profile = req.body.profile;
  profile.username = username;

  if (!profile.firstName || !profile.lastName || !profile.address1 || !profile.city || !profile.state || !profile.zipcode) {
    result = 'fail';
    res.json({
      result: result
    });
  }
  else if (profile.firstName.length > 24 || profile.lastName.length > 24 || profile.address1.length > 256 || profile.city.length > 128 || profile.zipcode.length > 64) {
    result = 'fail';
    res.json({
      result: result
    });
  }
  else
    profiles.find({
      username: username
    }, function(err, data) {
      if (data.length > 0){
        newProfile = data[0];
        newProfile.firstName = profile.firstName;
        newProfile.lastName = profile.lastName;
        newProfile.address1 = profile.address1;
        newProfile.address2 = profile.address2;
        newProfile.city = profile.city;
        newProfile.state = profile.state;
        newProfile.zipcode = profile.zipcode;

        newProfile.save(function(err, data) {
          mongoose.connection.close();

          if (err)
            result = 'fail';
          else
            result = 'success';

          res.json({
            result: result
          });
        });
      }
      else
        profiles.create(profile, function(err, data) {
          mongoose.connection.close();

          if (err)
            result = 'fail';
          else
            result = 'success';

          res.json({
            result: result
          });
        });
    });
});

module.exports = router;
