var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');

router.post('/post', function(req, res, next) {
  mongoose.connect('mongodb://localhost/eve', {useNewUrlParser: true});
  var db = mongoose.connection;

  var result = 'success'
  var quote = req.body.quote;

  var max_gallons = 999999;
  var max_date_len = 10;

  var fields_provided = quote.gallonRequested && quote.address && quote.date
  var valid = !isNaN(quote.gallonRequested) && (quote.gallonRequested <= max_gallons) && (quote.date.length <= max_date_len)

  if(!fields_provided || !valid) {
    result = 'fail';
    res.json({
      result: result,
      suggested: 0,
      total: 0
    });
  }
  else {
    var currentPrice = 1.5;

    var locationFactor = .04;
    if (req.body.profile.state == 'TX')
      locationFactor = .02;

    var historyFactor = 0.0;
    if (req.body.history.history.length > 0)
      historyFactor = .01;

    var gallonsRequestedFactor = .03;
    if (req.body.quote.gallonRequested > 1000)
      gallonsRequestedFactor = .02

    var companyProfitFactor = .1;

    var rateFluctuationFactor = .03;
    summer_start = '05-20';
    summer_end = '09-22';
    date_sub = req.body.quote.date.substring(0, 5);
    if((date_sub.localeCompare(summer_start) > -1) && (date_sub.localeCompare(summer_end) < 1))
      return 0.04

    var margin = currentPrice * ( locationFactor - historyFactor + gallonsRequestedFactor + companyProfitFactor + rateFluctuationFactor);

    var suggested = currentPrice + margin;
    suggested = suggested.toFixed(2);

    var total = quote.gallonRequested*suggested;

    res.json({
      result: result,
      suggested: suggested,
      total: total
    });
    /*
    var history = req.app.get('history');

    history.create({
      username: req.body.username,
      gallonRequested: req.body.quote.gallonRequested,
      address: req.body.quote.address,
      date: req.body.quote.date,
      suggested: suggested,
      total: total
    }, function(err, data) {
      mongoose.connection.close();

      if(err)
        result = 'fail';
      else {
        result = 'success';
      }

      res.json({
        result: result,
        suggested: suggested,
        total: total
      })
    });*/
  }
});

module.exports = router;
