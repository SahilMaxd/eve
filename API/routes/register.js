var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');

router.post('/post', function(req, res, next) {
  mongoose.connect('mongodb://localhost/eve', { useNewUrlParser: true});

  var result = '';

  var users = req.app.get('users');

  var user = req.body;

  if(!user.username || !user.password) {
    result = 'fail';
    res.json({
      result: result
    });
  }
  else if(user.username.length > 24 || user.password.length > 32) {
    result = 'fail';
    res.json({
      result: result
    });
  }
  else
    users.find({
      username: req.body.username
    }, function(err, data) {

      if (data.length > 0){
        result = 'fail';
        res.json({
          result: result
        });
      }
      else
        users.create({
          username: req.body.username,
          password: req.body.password
        }, function(err, data) {
          mongoose.connection.close();

          if (err)
            result = 'fail';
          else
            result = 'success';

          res.json({
            result: result
          });
        });
    });
});

module.exports = router;
