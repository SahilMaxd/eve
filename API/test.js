var request = require('supertest');
var app = require('./app');
var http = require('http');
var expect = require('chai').expect;

describe('API server', function() {
  var server;

  before(function(done) {
    server = app.listen(4400, function(err) {
        if (err)
          return done(err);
        done();
      });
  });

  it('should return successful with proper user account', function(done) {
    request(app)
      .post('/api/auth/')
      .send({username: 'admin', password: 'password'})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('success');
        done();
      });
  });

  it('should return fail with improper user account', function(done) {
    request(app)
      .post('/api/auth/')
      .send({username: '', password: ''})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('fail');
        done();
      });
  });

  it('should return json object containing profile', function(done) {
    request(app)
      .post('/api/profile/get/')
      .send({})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        done();
      });
  });

  it('should return successful with proper profile attributes', function(done) {
    request(app)
      .post('/api/profile/post/')
      .send({firstName: 'first', lastName: 'last', address1: '0000 Name Road', address2: '', city: 'city', state: 'TX', zipcode: '00000'})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('success');
        done();
      });
  });

  it('should return fail with blank profile attributes', function(done) {
    request(app)
      .post('/api/profile/post/')
      .send({firstName: '', lastName: '', address1: undefined, address2: '', city: 'city', state: 'TX', zipcode: '00000'})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('fail');
        done();
      });
  });

  it('should return fail with oversized profile attributes', function(done) {
    request(app)
      .post('/api/profile/post/')
      .send({firstName: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaa', lastName: 'bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb', address1: '0000 Name road', address2: '', city: 'city', state: 'TX', zipcode: '00000'})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('fail');
        done();
      });
  });

  it('should return json object containing populated quote information', function(done) {
    request(app)
      .post('/api/quote/get/')
      .send({})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        done();
      });
  });


  it('should return successful with proper quote attributes', function(done) {
    request(app)
      .post('/api/quote/post/')
      .send({gallonRequested: 8000, address: '0000 Name Road', date: '04-17-20', suggested: 1.50, total: 380.5})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('success');
        done();
      });
  });

  it('should return fail with blank quote attributes', function(done) {
    request(app)
      .post('/api/quote/post/')
      .send({gallonRequested: 0, address: '', date: '', suggested: 0, total: 0})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('fail');
        done();
      });
  });

  it('should return fail with oversized quote attributes', function(done) {
    request(app)
      .post('/api/quote/post/')
      .send({gallonRequested: 99999999999, address: '###############################################################################', date: '#################', suggested: 1.50, total: 380.5})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        var result = res.body.result;
        expect(result).to.equal('fail');
        done();
      });
  });


  it('should return json object containing quote history of user', function(done) {
    request(app)
      .post('/api/history/get/')
      .send({})
      .expect(200, function(err, res) {
        if(err)
          return done(err);
        done();
      });
  });

  after(function() {
    server.close();
  })
});
