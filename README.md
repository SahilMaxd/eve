To run Web server and API server:

1. Install Node.js
2. Globally install angular with npm
3. Clone 'eve' repository
4. Download mongodb server
5. Inside mongodb 'bin/' repository create 'data' folder
6. Inside mongodb 'bin/' repository run 'mongod --dbpath data' to store saved data in 'bin/data/' repository and keep this terminal open until you want to end the database server
7. Inside 'eve/API/' repository run 'npm install'
8. Inside 'eve/API/' repository run 'npm start' and keep this terminal open until you want to end the API server
9. Inside 'eve/web/' repository run 'npm install'
10. Inside 'eve/web/' repository run 'ng serve --open' and keep this terminal open until you want to end the Web server
11. On the website register user to enter for first time

Notes:
- '--open' flag in 'ng serve' command automatically opens up a browser to the link of the localhosted Web server
- otherwise, you can open a browser and go to link: 'http://localhost:4200'
- logout button is on the top right of the nav bar once logged in
- mongodb automatically uses 'data/db/' path in your root directory of machine if no --dbpath argument is specified, howeverr this 'data/db' repository has to be created before hand by the user
