import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './component/login/login.component';
import { RegistrationComponent } from './component/registration/registration.component';
import { ClientProfileComponent } from './component//client-profile/client-profile.component';
import { FuelQuoteComponent } from './component//fuel-quote/fuel-quote.component';
import { FuelQuoteHistoryComponent } from './component//fuel-quote-history/fuel-quote-history.component';
import { AuthgaurdService } from './service/authgaurd.service';

const routes: Routes = [
  { path: 'login', component: LoginComponent, canActivate: [AuthgaurdService]},
  { path: 'register', component: RegistrationComponent, canActivate: [AuthgaurdService]},
  { path: 'profile', component: ClientProfileComponent, canActivate: [AuthgaurdService]},
  { path: 'quote', component: FuelQuoteComponent, canActivate: [AuthgaurdService]},
  { path: 'history', component: FuelQuoteHistoryComponent, canActivate: [AuthgaurdService]},
  { path: '**', redirectTo: 'profile'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
