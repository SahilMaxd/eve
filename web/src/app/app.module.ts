import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './component/login/login.component';
import { RegistrationComponent } from './component/registration/registration.component';
import { ClientProfileComponent } from './component/client-profile/client-profile.component';
import { FuelQuoteComponent } from './component/fuel-quote/fuel-quote.component';
import { FuelQuoteHistoryComponent } from './component/fuel-quote-history/fuel-quote-history.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ClientProfileComponent,
    FuelQuoteComponent,
    FuelQuoteHistoryComponent,
    RegistrationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
