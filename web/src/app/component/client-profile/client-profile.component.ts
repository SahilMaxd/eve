import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Profile } from '../../interface/profile';

@Component({
  selector: 'app-client-profile',
  templateUrl: './client-profile.component.html',
  styleUrls: ['./client-profile.component.css']
})
export class ClientProfileComponent implements OnInit {
  API_URL: string = 'http://localhost:4400/api';
  firstName: string = '';
  lastName: string = '';
  address1: string = '';
  address2: string = '';
  city: string = '';
  state: string = '';
  zipcode: string = '';

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.getProfile();
  }

  getProfile() {
    this.http.post(this.API_URL + '/profile/get', { username: localStorage.getItem('currentUser') })
      .subscribe((data: Profile) => {
        this.firstName = data.firstName;
        this.lastName = data.lastName;
        this.address1 = data.address1;
        this.address2 = data.address2;
        this.city = data.city;
        this.state = data.state;
        this.zipcode = data.zipcode;
      });
  }

  postProfile() {
    if (this.firstName && this.lastName && this.address1 && this.city && this.state != 'Select State' && this.zipcode) {
      let profile: Profile = {
        firstName: this.firstName,
        lastName: this.lastName,
        address1: this.address1,
        address2: this.address2,
        city: this.city,
        state: this.state,
        zipcode: this.zipcode
      }
      this.http.post(this.API_URL + '/profile/post', { username: localStorage.getItem('currentUser'), profile: profile })
        .subscribe((data: any) => {
          console.log(data.result);
        })
    }
    else
      console.log('make sure required fields are entered');
  }
}
