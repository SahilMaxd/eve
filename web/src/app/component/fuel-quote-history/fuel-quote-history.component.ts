import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Quote } from '../../interface/quote';
import { History } from '../../interface/history';

@Component({
  selector: 'app-fuel-quote-history',
  templateUrl: './fuel-quote-history.component.html',
  styleUrls: ['./fuel-quote-history.component.css']
})
export class FuelQuoteHistoryComponent implements OnInit {
  API_URL: string = 'http://localhost:4400/api';
  history: Quote[] = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.getHistory();
  }

  getHistory() {
    this.http.post(this.API_URL + '/history/get', { username: localStorage.getItem('currentUser') })
      .subscribe((data: History) => {
        this.history = data.history;
      });
  }

}
