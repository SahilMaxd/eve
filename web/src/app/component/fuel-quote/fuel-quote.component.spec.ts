import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuelQuoteComponent } from './fuel-quote.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('FuelQuoteComponent', () => {
  let component: FuelQuoteComponent;
  let fixture: ComponentFixture<FuelQuoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuelQuoteComponent ],
      imports: [ HttpClientTestingModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuelQuoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
