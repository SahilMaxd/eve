import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Profile } from '../../interface/profile';
import { History } from '../../interface/history';
import { Quote } from '../../interface/quote';

@Component({
  selector: 'app-fuel-quote',
  templateUrl: './fuel-quote.component.html',
  styleUrls: ['./fuel-quote.component.css']
})
export class FuelQuoteComponent implements OnInit {
  API_URL: string = 'http://localhost:4400/api';

  gallonRequested: number = 0;
  address: string = ''
  date = {
    month: '',
    day: '',
    year: ''
  };
  newdate: string = '';
  suggested: number = 0;
  total: number = 0;

  firstName: string = '';
  lastName: string = '';
  address1: string = '';
  address2: string = '';
  city: string = '';
  state: string = '';
  zipcode: string = '';

  history: Quote[] = [];

  isDisabled: boolean = true;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.getData();
    //this.getQuote();
  }

  getData() {
    this.http.post(this.API_URL + '/profile/get', { username: localStorage.getItem('currentUser')})
      .subscribe((data: Profile) => {
        this.firstName = data.firstName;
        this.lastName = data.lastName;

        this.address = data.address1;

        this.address1 = data.address1;
        this.address2 = data.address2;
        this.city = data.city;
        this.state = data.state;
        this.zipcode = data.zipcode;

        this.http.post(this.API_URL + '/history/get', { username: localStorage.getItem('currentUser') })
          .subscribe((data: History) => {
            this.history = data.history;

            this.isDisabled = false;
          });
      });
  }

  getQuote() {
    var max_gallons = 999999;
    var max_date_len = 10;

    var fields_provided = this.gallonRequested && this.address && this.date
    var valid = !isNaN(this.gallonRequested) && (this.gallonRequested <= max_gallons)

    var date = ('0' + this.date.month.toString()).slice(-2) + '-' + ('0' + this.date.day).slice(-2) + '-' + this.date.year

    if(!fields_provided) {
      console.log('make sure required fields are entered')
      return
    }

    if(!valid) {
      console.log('make sure fields are the correct type')
      return
    }


    let quote: Quote = {
      gallonRequested: this.gallonRequested,
      address: this.address,
      date: date,
      suggested: this.suggested,
      total: this.total
    }
    let profile: Profile = {
      firstName: this.firstName,
      lastName: this.lastName,
      address1: this.address1,
      address2: this.address2,
      city: this.city,
      state: this.state,
      zipcode: this.zipcode,
    }
    let history: History = {
      history: this.history
    }
    this.http.post(this.API_URL + '/quote/post', { username: localStorage.getItem('currentUser'), quote: quote, profile: profile, history: history })
      .subscribe((data: any) => {
        if(data.result == 'fail')
          console.log('failed to predict price');
        else
          console.log(typeof data.suggested, typeof data.total);
          this.suggested = parseFloat(data.suggested);
          this.total = data.total;
      })
    }

    postQuote() {
      var max_gallons = 999999;
      var max_date_len = 10;

      var fields_provided = this.gallonRequested && this.address && this.date
      var valid = !isNaN(this.gallonRequested) && (this.gallonRequested <= max_gallons)

      var date = ('0' + this.date.month.toString()).slice(-2) + '-' + ('0' + this.date.day).slice(-2) + '-' + this.date.year

      if(!fields_provided) {
        console.log('make sure required fields are entered')
        return
      }

      if(!valid) {
        console.log('make sure fields are the correct type')
        return
      }


      let quote: Quote = {
        gallonRequested: this.gallonRequested,
        address: this.address,
        date: date,
        suggested: this.suggested,
        total: this.total
      }
      this.http.post(this.API_URL + '/history/post', { username: localStorage.getItem('currentUser'), quote: quote })
        .subscribe((data: any) => {
          if(data.result == 'fail')
            console.log('failed to predict price');
          else
            console.log('success');
        })
      }
  }
