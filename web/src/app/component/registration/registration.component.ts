import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AuthService } from '../../service/auth.service';
import { User } from '../../interface/user';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  API_URL: string = 'http://localhost:4400/api';
  username: string;
  password: string;

  constructor(private authService: AuthService, private http: HttpClient) { }

  ngOnInit(): void {
  }

  register() {
    if (this.username && this.password) {
      let user: User = {
        username: this.username,
        password: this.password
      };

      this.http.post(this.API_URL + '/register/post', user)
        .subscribe((data: any) => {
          if (data.result == 'fail')
            console.log('Failure to store user credentials');
          else {
            console.log('Successfully stored user credentials');
            this.authService.login(user);
          }
        });
    }
    else
      console.log('make sure username and password fields are entered');
  }

}
