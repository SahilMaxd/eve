export interface Quote {
  gallonRequested: number;
  address: string;
  date: string;
  suggested: number;
  total: number;
}
