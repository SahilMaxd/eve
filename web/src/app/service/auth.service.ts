import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { User } from '../interface/user';
import { AuthResponse} from '../interface/authResponse';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  loggedIn: BehaviorSubject<boolean>;
  API_URL: string = 'http://localhost:4400/api';

  constructor(private http: HttpClient, private router: Router) {
    this.initialize();
  }

  initialize() {
    if (localStorage.getItem('currentUser'))
      this.loggedIn = new BehaviorSubject<boolean>(true);
    else
      this.loggedIn = new BehaviorSubject<boolean>(false);
  }

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  login(user: User) {
    if (user.username && user.password)
      this.http.post(this.API_URL + '/auth', user)
        .subscribe((data: AuthResponse) => {
          if (data.result == 'success') {
            localStorage.setItem('currentUser', data.currentUser);
            this.loggedIn.next(true);
            this.router.navigate(['/']);
          }
          else {
            console.log('There is no user with those credentials');
          }
        });
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.loggedIn.next(false);
    this.router.navigate(['/login'])
  }
}
