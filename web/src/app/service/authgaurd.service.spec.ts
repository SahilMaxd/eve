import { TestBed } from '@angular/core/testing';

import { AuthgaurdService } from './authgaurd.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('AuthgaurdService', () => {
  let service: AuthgaurdService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ]
    });
    service = TestBed.inject(AuthgaurdService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
