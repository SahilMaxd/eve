import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthgaurdService implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (state.url == '/login' || state.url == '/register'){
      if (!localStorage.getItem('currentUser'))
        return true;
      this.router.navigate(['/']);
      return false;
    }
    if (localStorage.getItem('currentUser'))
      return true;
    this.router.navigate(['/login']);
    return false;
  }
}
